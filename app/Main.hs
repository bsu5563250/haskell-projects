module Main (main) where

import Lib

main :: IO ()
main = do
  -- gcd
  let a = 48
      b = 18
  putStrLn ("The GCD of " ++ show a ++ " and " ++ show b ++ " is " ++ show (gcd a b))
  -- Нахождение Fib(n) за log(n)
  let matrix = ((1, 1), (1, 0))
  let exponent = 7
  let result = fastMatrixExponentiation matrix exponent
  putStrLn $ "Fib("++ show exponent ++ ") = " ++ show (snd (fst result))
  -- Проверка на совершенство
  let number = 28
  putStrLn ("Is " ++ show number ++ " a perfect number? " ++ show (isPerfect number))
  -- Вывести длину сиракузской последовательности
  let n = 7
  putStrLn ("Count of siracuz sequence for n=7: "++show (collatzLength n))
  --
  let a = 2
      b = 2
  print (delannoy a b)  
  -- 
  let coeffs = [2, 1, 5]  -- Represents the polynomial 2x^2 + x + 5
      x = 3
  putStrLn $ "The value of the polynomial at x = " ++ show x ++ " is " ++ show (evalPolynomial coeffs x)
  --
  putStrLn "Before cloning: [1, 2, 3]"
  putStrLn $ "After cloning 3 times: " ++ show (clone 3 [1, 2, 3])
  putStrLn "Before cloning: [1, 2, 3]"
  putStrLn $ "After cloning 1 time: " ++ show (clone 1 [1, 2, 3])
  putStrLn "Before cloning: [1, 2, 3]"
  putStrLn $ "After cloning 0 times: " ++ show (clone 0 [1, 2, 3])
  --
  print (xZipWith (+) [10, 20, 30] [9, 8, 7])
  print (xZipWith (+) [10, 20, 30] [9, 8, 7, 6, 5, 4])
  print (xZipWith (+) [10, 20, 30] [])
  -- fibo
  print $ take 10 $ generalizedFibonacci [7, 3, 10, 0]
  -- системы счисления
  print $ fromDigits 2 [1, 0, 1, 1, 0, 1] 
  print $ toDigits 2 45  
  print $ addDigitwise 2 [1, 0, 1, 1, 0, 1] [1, 1, 1] 
  -- Перечисление путей в решётке
  print $ delannoyPaths 2 2
  
