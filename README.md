# my-project
## Задачи из презентации 04
## Вывод функций:
The GCD of 48 and 18 is 6
Fib(7) = 13
Is 28 a perfect number? True
Count of siracuz sequence for n=7: 17
13
The value of the polynomial at x = 3 is 50
Before cloning: [1, 2, 3]
After cloning 3 times: [1,1,1,2,2,2,3,3,3]
Before cloning: [1, 2, 3]
After cloning 1 time: [1,2,3]
Before cloning: [1, 2, 3]
After cloning 0 times: []
[19,28,37]
[19,28,37]
[]
[7,3,10,0,20,33,63,116,232,444]
45
[1,0,1,1,0,1]
[1,1,0,1,0,0]
[[0,0,2,2],[0,1,2],[0,2,0,2],[0,2,1],[0,2,2,0],[1,0,2],[1,1],[1,2,0],[2,0,0,2],[2,0,1],[2,0,2,0],[2,1,0],[2,2,0,0]]