module Lib
    ( fastMatrixExponentiation, mygcd, isPerfect, collatzLength, delannoy, evalPolynomial, clone, xZipWith, generalizedFibonacci
    , fromDigits, toDigits, addDigitwise, delannoyPaths
    ) where

import Data.Array
import Data.List (unfoldr)

-- gcd
mygcd :: Integral a => a -> a -> a
mygcd a 0 = a
mygcd a b = gcd b (a `mod` b)

-- Нахождение Fib(n) за log(n)
type Matrix2x2 = ((Int, Int), (Int, Int))

-- Функция для умножения двух матриц 2x2
multiplyMatrices :: Matrix2x2 -> Matrix2x2 -> Matrix2x2
multiplyMatrices ((a, b), (c, d)) ((e, f), (g, h)) =
  ((a * e + b * g, a * f + b * h), (c * e + d * g, c * f + d * h))

-- Функция для возведения матрицы 2x2 в степень за log(n) время
fastMatrixExponentiation :: Matrix2x2 -> Int -> Matrix2x2
fastMatrixExponentiation _ 0 = ((1, 0), (0, 1))
fastMatrixExponentiation matrix n
  | even n    = half `multiplyMatrices` half
  | otherwise = matrix `multiplyMatrices` (half `multiplyMatrices` half)
  where
    half = fastMatrixExponentiation matrix (n `div` 2)

-- Calculate the sum of divisors of a number
sumOfDivisors :: Integral a => a -> a
sumOfDivisors n = sum [x | x <- [1..n-1], n `mod` x == 0]

-- Check if a number is perfect
isPerfect :: Integral a => a -> Bool
isPerfect n = n == sumOfDivisors n

-- This function calculates the next number in the Collatz sequence.
collatzNext :: Integral a => a -> a
collatzNext n
  | even n    = n `div` 2
  | otherwise = 3 * n + 1

-- This function calculates the length of the Collatz sequence for a given number.
collatzLength :: Integral a => a -> Int
collatzLength n = length (takeWhile (>1) (iterate collatzNext n)) + 1


-- delannoy function with memoization.
delannoy :: Integer -> Integer -> Integer
delannoy m n = memo ! (m, n)
  where
    bounds = ((0, 0), (m, n))
    memo = listArray bounds [del m n | (m, n) <- range bounds]
    del 0 k = 1
    del m 0 = 1
    del m n = memo ! (m - 1, n) + memo ! (m, n - 1) + memo ! (m - 1, n - 1)

-- Evaluate the polynomial with coefficients given in the list at the value x.
evalPolynomial :: Num a => [a] -> a -> a
evalPolynomial coeffs x = foldr (\coef acc -> acc * x + coef) 0 coeffs

-- clone
clone :: Int -> [a] -> [a]
clone n = concatMap (replicate n)

-- xZipWith
xZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
xZipWith _ [] _ = []
xZipWith _ _ [] = []
xZipWith f (x:xs) (y:ys) = f x y : xZipWith f xs ys

-- Fibo
fibonacci :: Int -> [Integer]
fibonacci n = take n (map fst $ iterate (\(a, b) -> (b, a + b)) (0, 1))

infiniteFibonacci :: [Integer]
infiniteFibonacci = map fst $ iterate (\(a, b) -> (b, a + b)) (0, 1)

generalizedFibonacci :: [Integer] -> [Integer]
generalizedFibonacci start = start ++ nextFibs start
  where
    nextFibs nums = let next = sum (take (length start) nums) in next : nextFibs (tail nums ++ [next])

-- системы счисления

-- Вычисление числа из списка его цифр
fromDigits :: Int -> [Int] -> Int
fromDigits n = foldl (\acc x -> acc * n + x) 0

-- Преобразование числа в список его цифр
toDigits :: Int -> Int -> [Int]
toDigits n = reverse . unfoldr (\x -> if x == 0 then Nothing else Just (x `mod` n, x `div` n))

-- Сложение двух чисел, представленных списками их цифр
addDigitwise :: Int -> [Int] -> [Int] -> [Int]
addDigitwise n xs ys = toDigits n (fromDigits n xs + fromDigits n ys)

-- пути

-- Генерация всех путей в решетке a x b.
delannoyPaths :: Int -> Int -> [[Int]]
delannoyPaths a b = generatePaths a b 0 0

-- Вспомогательная функция для генерации путей.
generatePaths :: Int -> Int -> Int -> Int -> [[Int]]
generatePaths a b right up
  | right == a && up == b = [[]]
  | right == a = map (2 :) $ generatePaths a b right (up + 1)
  | up == b = map (0 :) $ generatePaths a b (right + 1) up
  | otherwise = map (0 :) (generatePaths a b (right + 1) up) ++
                map (1 :) (generatePaths a b (right + 1) (up + 1)) ++
                map (2 :) (generatePaths a b right (up + 1))
